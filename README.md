# A Prolog Interpreter written in Haskell
This is a simple interpreter for a subset of the [Prolog programming language](https://en.wikipedia.org/wiki/Prolog) written in [Haskell](https://en.wikipedia.org/wiki/Haskell_%28programming_language%29).  
It uses the Parsing library from *chapter 13 of Programming in Haskell, Graham Hutton, Cambridge University Press, 2016.*

## Current work:
It currently does not backtrack on the unification: the first unification is the only one considered.
It currently support parsing simple prolog *Knowledge Bases* and *Goals* without *cuts*:  
Examples:

- Knowlegde Bases
  - `"gatto(tom). miagola(tom):-gatto(tom)."`
  - `"tom. gatto(tom). miagola(tom):-gatto(tom)."`
- Goals
  - `"tom"`
  - `"miagola(tom)"`
- Full Example:
  + **Knowledge Base**: `"gatto(tom). miagola(tom):-gatto(tom)."`
  + **Goal**: `"miagola(tom)"`
  + **Result**: `True`
 

fixed: (now it can interpret them correctly)
> It can also parse *variables*:
> 
> - `"tom. gatto(tom). miagola(X):-gatto(X)."`
>   
> But it cannot interpret them:
> 
>  - Example:
>    + **Knowledge base**: `"tom. gatto(tom). miagola(X):-gatto(X)."`
>    + **Goal**: `"miagola(tom)"`
>    + **Result**: `False`

## Examples: (from the most interesting to the least interesting)

```Haskell
tresolve2 "miagola(X):-gatto(X).felice(X):-sazio(X).felice(X):-t,miagola(X).gatto(tomm).t." "felice(tomm)"
> True
```

```Haskell
tresolve2 "gatto(tom).miagola(X):-gatto(X).abbaia(X):-cane(X)." "miagola(tom)"
> True
```

```Haskell
tresolve "gatto(tom). miagola(tom):-gatto(tom)." "miagola(tom)"
> True
```

```Haskell
tresolve "gatto(tom). miagola(tom):-gatto(tom)." "tom"
> False
```

> To run this examples use the function `tresolve`.  
> The first parameter is a `string` containing the *Knowledge Base*,  
> the second parameter is a `string` containing the *goal*. 
# :construction_site: How to try it

1. Download this directory.
1. open a terminal inside this directory.
1. run `ghci main.hs`
1. run one of the examples:
   ```Haskell
   tresolve "gatto(tom). miagola(tom):-gatto(tom)." "miagola(tom)"
   ```

# Features
- [x] parser
  -  [ ] cuts 
- [x] resolution algorithm on terms
- [x] unifier algorithm
- [x] substitution algorithm
- [x] resolution algorithm on variables
### features possibly out of scope
- [ ] lists
- [ ] operators
- [ ] negation operator
- [ ] cut

# To Do
- [ ] fix the whitespace (parser)

