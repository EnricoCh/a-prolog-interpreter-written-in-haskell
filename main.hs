import Parsing
import Data.List
import Control.Monad
import Data.Maybe
import Debug.Trace


type Name = String

newtype VariableName = VariableName String
 deriving(Show,Eq)

data Value = Variable VariableName
           | Term Name [Value]
 deriving(Eq)

data Fact = Simple Value
          | Impl Value [Value] -- Head first, Tail second

type Goal = Value

type KnowledgeBase = [Fact]

instance Show Fact where
    show (Simple var) = show var
    show (Impl head tail) = show head ++ "->" ++ concatMap show tail

instance Show Value where
    show (Variable (VariableName varname)) = varname
    show (Term predicate tail) = predicate ++ "(" ++ concatMap show tail ++ ")"

-- Parser

variableP = do
    x <- upper
    xs <- many alphanum
    return $ Variable $ VariableName (x:xs)

simpleTermP = do
    x <- lower
    xs <- many alphanum
    return $ Term (x:xs) []

complexTermP = do
    x <- lower
    xs <- many alphanum
    char '('
    arg <- valueP
    args <- many $ char ',' >> valueP
    char ')'
    return $ Term (x:xs) (arg:args)

valueP = variableP <|> complexTermP <|> simpleTermP

simpleFactP = do
    v <- valueP
    char '.'
    return $ Simple v

implFactP = do
    head <- valueP
    string ":-"
    tail <- valueP
    tails <- many $ char ',' >> valueP
    char '.'
    return $ Impl head (tail:tails)

factP = simpleFactP <|> implFactP

knowledgeBaseP =
    many $ do
            f <- factP
            space
            return f

replP = valueP

    -- utilities

parseBase s = let [(kw,_)] = parse knowledgeBaseP s in kw --"tom.gatto(tom)."

parseGoal s = let [(goal,_)] = parse replP s in [goal]

parseRepl s = let [(repl,_)] = parse replP s in repl

-- end parser

-- utilities
mkvar = Variable . VariableName

-- Unifier Aglorithm

type Equation = (Value,Value)

termReduction :: (Value, Value) -> Maybe [(Value, Value)]
termReduction
    (Term f1 [], Term f2 []) =
        if f1 == f2 then Just [] else Nothing
termReduction
    (Term f1 _, Term f2 []) = Nothing
termReduction
    (Term f1 [], Term f2 _) = Nothing
termReduction
    (Term f1 list1, Term f2 list2) =
        if f1 == f2
            then
                let zipped = zip list1 list2
                    a = map termReduction zipped :: [Maybe [(Value, Value)]]
                in
                    if Nothing `elem` a
                        then Nothing
                        else
                            let b = sequence a :: Maybe [[(Value, Value)]]
                                c = Just $ concat $ fromJust b
                            in c
            else Nothing
termReduction
    (v@(Variable v1), t@(Term _ _)) =
        Just [(v,t)]
termReduction
    (t@(Term _ _),v@(Variable v1)) =
        Just [(v,t)]
termReduction
    (vv1@(Variable v1),vv2@(Variable v2)) =
        Just [(vv1,vv2)]


ttest = termReduction (Term "gatto" [Term "tom" [] ], Term "gatto" [Term "tom" []])

-- utility function to test termReduction
tred x y = termReduction (parseRepl x, parseRepl y)


-- end unifier


-- resolver

tailOfFact (Simple _) = []
tailOfFact (Impl _ tail) = tail


-- sostituire l'ugualianza con l'unificabilità
-- la funzione prende il parametro 1 (di tipo Value) 
--  e il parametro 2 (di tipo Fact) e restituisce 
-- true o false se unificano o no e anche con quali assegnazioni unificano
-- il tipo di ritorno puo essere Maybe [assegnazione]
-- con Nothing se non unificano
-- Just [assegnazioni] se unificano con [assegnazioni]
-- L'unificazione è data dalla funzione termReduction

hasHead :: Value -> Fact -> Maybe [(Value, Value)]
hasHead x (Simple val) = termReduction (x, val)
hasHead x (Impl val _) = termReduction (x, val)


tresolve2 :: String -> String -> Bool
tresolve2 base goal = resolve2 (parseBase base) (parseGoal goal)
    -- example : tresolve "gatto(tom).miagola(tom):-gatto(tom)." "miagola(tom)"

        -- example
    -- tresolve2 "gatto(tomm).miagola(tom):-gatto(tom).abbaia(X):-cane(X)." "miagola(tom)"
resolve2 :: [Fact] -> [Value] -> Bool
resolve2 kb [] = True
resolve2 kb (x:xs) =
    let clausesToUse =
                        filter
                        (isJust . fst)
                        $ map
                            (\v -> (hasHead x v, v))
                            kb
                            :: [(Maybe [(Value, Value)], Fact)]
        clausesWithoutNothings = traceShow  ("clausesToUse>" ++ show clausesToUse) $
                removeNothings clausesToUse
        clausesToUse2 =
                map (\(x,y) -> performSub y x) clausesWithoutNothings
        tails = traceShow ("clausesTouse2>" ++ show clausesToUse2) $
                map tailOfFact clausesToUse2 -- this takes the second element of the tuple ( the rule to apply)
                                      -- then perform the sobstitution present in the first element of the tuple
                                      -- then takes the tail of fact
        newGoals = map ( ++ xs ) tails
        d =
                map (resolve2 kb) newGoals
        res = traceShow ("newGoals>" ++ show newGoals) $ or d -- true iff there is at least one True in the list
    in traceShow ("resolving->" ++ show x) $ res

removeNothings :: [(Maybe a, b)] -> [(a, b)]
removeNothings [] = []
removeNothings ((Nothing, _) : xs) = removeNothings xs
removeNothings ((Just  a, b) : xs) = (a, b) : removeNothings xs

performSub :: Fact -> [(Value, Value)] -> Fact
performSub (Simple value) dict = Simple $ replaceName value dict
    -- head is not important , it will not be used, but lets perform the sub on the head as well
performSub (Impl head tail) dict = Impl (replaceName head dict) (map (`replaceName` dict) tail)

ptest = performSub 
        (Impl (Term "miagola" [mkvar "X"]) 
              [Term "gatto" [mkvar "X"]]) 
        ([(mkvar "X", Term "j" [])])
test = map (`replaceName` ([(mkvar "X", mkvar "Y")])) [mkvar "X"]
rtest = replaceName (Term "gatto" [mkvar "X"]) ([(mkvar "X", mkvar "Y")])
test2 = let dict = [(mkvar "X", mkvar "jj"), (mkvar "asdda", Term "gatto" [mkvar "kk"])]
            tail = [Term "gatto" [mkvar "X"]]
        in (map (`replaceName` dict) tail)
test3 = let dict = [(mkvar "X", mkvar "jj"), (mkvar "T", Term "gatto" [mkvar "kk"])]
            tail = [Term "g" [], mkvar "T"]
        in (map (`replaceName` dict) tail)

replaceName :: Value ->  [(Value, Value)] -> Value
replaceName val@(Variable _) dict =
        let el = find ((val ==) . fst) dict
        in
            case el of
                Nothing -> val
                Just (old, new) -> replaceName new dict
replaceName val@(Term termName tail) dict = Term termName $ map (\x -> replaceName x dict) tail  

        -- examples
    -- replaceName (mkvar "kk") [(mkvar "asdd", mkvar "jj"), (mkvar "asdda", Term "gatto" [mkvar "kk"])]
    -- replaceName (mkvar "kk") [(mkvar "asdd", mkvar "jj"), (mkvar "asdda", Term "gatto" [mkvar "kk"])]
    -- replaceName (mkvar "asdd") [(mkvar "asdd", mkvar "jj")]
    -- replaceName (mkvar "asdda") [(mkvar "asdd", mkvar "jj")]

-- end resolver